INTRODUCCIÓN
------------

Éste es un tema instalable para Plone 3.0 diseñado en base al estilo
de la Metadistribución Canaima GNU/Linux, desarrollado y mantenido por
el equipo de desarrollo web de la Metadistribución [1].

Canaima Aponwao está basado en el tema para Wordpress "FREEMium" [2] y
en el Producto para Plone "quintagroup.theme.techlight" [3], en estilo
y sistema de archivos respectivamente.


[1] Luis Martínez (HuntingBears)
    Leonardo Caballero (macagua)
    Xavier Araque (Rendergraf)
    
[2] Tema bajo licencia GPL
    Desarrollado por Paul Kadysz, Dariusz Siedlecki
    Disponible en http://www.freebiesdock.com/

[3] Tema bajo licencia GPL
    Desarrollado por Y. Hvozdovych, N. Ilchuk, V. Rudnytskyy
    Disponible en http://skins.quintagroup.com/techlight
    Contacto en skins@quintagroup.com

Más Información: canaima.aponwaotheme.egg-info/PKG-INFO
